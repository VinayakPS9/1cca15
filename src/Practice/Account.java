package Practice;
//STEP-1
public interface Account {
    void deposit(double amt);
    void withdraw(double amt);
    void checkBalance();
}
