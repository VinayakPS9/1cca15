package Practice;
//STEP-2

public class LoanAccount implements Account{
    double loanAmount;
    //account creation
    public LoanAccount(double loanAmount){
        this.loanAmount = loanAmount;
        System.out.println("Loan Account Created");
    }

    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+" Rs Debited From Your Loan Account");
    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+" Rs Credited to Your Account");
    }

    @Override
    public void checkBalance() {
        System.out.println("ACTIVE LOAN AMOUNT "+loanAmount);
    }
}
